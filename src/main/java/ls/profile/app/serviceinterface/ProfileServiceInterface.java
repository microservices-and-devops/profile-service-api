package ls.profile.app.serviceinterface;

import java.util.Collection;

import ls.profile.app.entity.Profile;

public  interface ProfileServiceInterface {
	   public abstract void createProfile(Profile product);
	   public abstract void updateProfile(Long id, Profile profile);
	   public abstract void deleteProfile(Long id);
	   public abstract Collection<Profile> getProfile();
}
