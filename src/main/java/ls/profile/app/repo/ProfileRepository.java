package ls.profile.app.repo;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ls.profile.app.entity.Profile;
@Repository
public interface ProfileRepository extends PagingAndSortingRepository<Profile, Long>{
	List<Profile> findAll();
	List<Profile> findByIdIn(List<Long> id);
}
