package ls.profile.app.dataSeeder;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import ls.profile.app.repo.ProfileRepository;

@Component
public class Profile implements CommandLineRunner{
	@Autowired 
	ProfileRepository profileRepository;
	
	int MAX_ITEM = 200;
	@Override
	public void run(String... args) throws Exception {
		Faker faker = new Faker(new Random(50));
		
		
		for (int i=0; i < MAX_ITEM; i++) {
			ls.profile.app.entity.Profile profile = new ls.profile.app.entity.Profile();
			profile.setProfileCode(String.valueOf(faker.number().numberBetween(1, 1000)));
			profile.setFirstName(faker.address().firstName());
			profile.setLastName(faker.address().lastName());
			profile.setAddress(faker.address().streetAddress());
			profile.setCity(faker.address().city());
			profile.setEmail(faker.address().firstName().concat("@gmail.com"));
			profile.setMobileNo(faker.phoneNumber().cellPhone());
			profile.setDeleteFlag("false");
			profile.setCreatedAt(faker.date().birthday());
			profile.setUpdatedAt(faker.date().birthday());
			
			profileRepository.save(profile);
		}
	}

}
